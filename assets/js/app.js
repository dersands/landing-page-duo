

$(document).ready(function () {
    /* accordion */
    $('.accordion .item-anchor').click(function (e) {
        e.preventDefault();
        var item = $(this).parent();
        
        if(!item.hasClass('active')) {
            item.parent().find('.active').removeClass('active').find('.item-content').slideToggle(300);
        }

        item.toggleClass('active');
        item.find('.item-content').slideToggle(300).promise().done(function(){
            
            if($(this).parent().hasClass('active')) {
                $([document.documentElement,document.body]).animate({scrollTop:$(this).parent().offset().top - 200}, 400);
            }
        });
        
    });
    /* accordion */


    /* video carousel */
    $('.video-carousel').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false,
        dots: false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '60px',
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            }]
    });
    /* video carousel */
    
    $('.video-carousel').on('click', 'a', function(e){
        e.preventDefault();
        $('#video-iframe').attr('src', $(this).attr('href'));
    });
    
    
    /* scroll event to hide and show bar */
    $(window).scroll(checkScroll);
    $(window).resize(checkScroll);
    
    function checkScroll() {
        if(window.innerWidth > 767) {
            var threshold = $('#display').height() + $('#display').offset().top;
            console.log($(document).scrollTop());

            if($(document).scrollTop() > threshold && !$('body').hasClass('show-bar')) {
                $('body').addClass('show-bar');
            } else
            if($(document).scrollTop() < threshold && $('body').hasClass('show-bar')) {
                $('body').removeClass('show-bar');
            }
        }
    }
});